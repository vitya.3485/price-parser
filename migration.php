<?php

/**
 * File for table creation
 */

include 'vendor/autoload.php';

use Viktor\OlxParser\DB\Connection;

$connection = Connection::getInstance();

$sql = <<<SQL

CREATE TABLE IF NOT EXISTS email (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS advert (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    advert VARCHAR(500) NOT NULL UNIQUE,
    old_price VARCHAR(50),
    new_price VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS email_advert (
    email_id INT UNSIGNED NOT NULL,
    advert_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (email_id) REFERENCES email(id) ON DELETE CASCADE,
    FOREIGN KEY (advert_id) REFERENCES advert(id) ON DELETE CASCADE,
    UNIQUE KEY email_id_advert_id(email_id, advert_id)
);

SQL;

$connection->exec($sql);
