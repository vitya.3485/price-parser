<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Services\Cron;

/**
 * Request price for url
 *
 * @author vitya.3485@gmail.com
 */
class RequestPrice
{
    /**
     * Select price node from website
     *
     * @param string $url
     *
     * @return string
     */
    public function execute(string $url): string
    {
        $page   = file_get_contents($url);
        $config = include __DIR__ . '/../../config.php';
        $start  = strpos($page, $config['olx_selector_start']);
        $end    = strpos($page, $config['olx_selector_end']);

        if ($start !== false && $end !== false && $start < $end) {
            $startLen = strlen((string) $config['olx_selector_start']);
            $startPos = $start + $startLen;
            $endPos   = $end - $start - $startLen;
            $result   = substr($page, $startPos, $endPos);

            if (!empty($result)) {
                return $result;
            }
        }

        return 'not available to select';
    }
}
