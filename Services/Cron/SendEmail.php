<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Services\Cron;

/**
 * Send emails
 *
 * @author vitya.3485@gmail.com
 */
class SendEmail
{
    /**
     * Email content subject and text
     */
    private const SUBJECT = 'The price of observed advert was changed!';
    private const CONTENT = 'The new price of advertisement is <b> %1$s </b>';

    /**
     * Send email to subscribed email
     *
     * @param string $email
     * @param string $price
     *
     * @return bool
     */
    public function execute(string $email, string $price): bool
    {
        $config = include __DIR__ . '/../../config.php';

        $headers = "From: " . $config['email']['email_from'] . "\r\n";
        $headers .= "Reply-To: " . $config['email']['email_from'] . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8 \r\n";

        return mail($email, self::SUBJECT, sprintf(self::CONTENT, $price), $headers);
    }
}
