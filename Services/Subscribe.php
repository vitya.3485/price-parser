<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Services;

use Exception;
use Viktor\OlxParser\Entity\Advert;
use Viktor\OlxParser\Entity\Email;
use Viktor\OlxParser\Entity\EmailAdvert;
use Viktor\OlxParser\Services\Cron\RequestPrice;

/**
 * Subscribe new user to an email
 *
 * @author vitya.3485@gmail.com
 */
class Subscribe
{
    /**
     * @var Advert
     */
    private readonly Advert       $advert;

    /**
     * @var RequestPrice
     */
    private readonly RequestPrice $price;

    /**
     * @var Email
     */
    private readonly Email        $email;

    /**
     * @var EmailAdvert
     */
    private readonly EmailAdvert  $emailAdvert;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->advert      = new Advert();
        $this->price       = new RequestPrice();
        $this->email       = new Email();
        $this->emailAdvert = new EmailAdvert();
    }

    /**
     * Subscribe new email or return false
     *
     * @param string $email
     * @param string $link
     *
     * @return bool
     */
    public function execute(string $email, string $link): bool
    {
        try {
            $price    = $this->price->execute($link);
            $advertId = $this->advert->save($link, $price, $price);
            $emailId  = $this->email->save($email);

            $this->emailAdvert->save($emailId, $advertId);
        } catch (Exception) {
            return false;
        }
        return true;
    }
}
