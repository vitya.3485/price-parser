<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Services;

use Viktor\OlxParser\DB\Connection;
use Viktor\OlxParser\Services\Cron\SendEmail;
use Viktor\OlxParser\Services\Cron\RequestPrice;
use PDO;
use PDOException;

/**
 * Subscribe new user to an email
 *
 * @author vitya.3485@gmail.com
 */
class CronTask
{
    /**
     * Sql queries for update database and sending email
     */
    private const MOVE_NEW_TO_OLD = 'UPDATE advert SET old_price = new_price;';
    private const SELECT_ADVERTS = 'SELECT * FROM advert;';
    private const INSERT_ADVERTS = 'UPDATE advert SET new_price = :new_price WHERE id = :id;';
    private const SELECT_EMAILS  = <<<SELECT_EMAILS
        SELECT email.email, advert.new_price FROM advert
        JOIN email_advert ON email_advert.advert_id = advert.id
        JOIN email ON email_advert.email_id = email.id
        WHERE old_price != new_price;
    SELECT_EMAILS;

    /**
     * @var SendEmail
     */
    private readonly SendEmail    $sender;

    /**
     * @var RequestPrice
     */
    private readonly RequestPrice $request;

    /**
     * @var PDO
     */
    private readonly PDO          $connection;

    public function __construct()
    {
        $this->sender     = new SendEmail();
        $this->request    = new RequestPrice();
        $this->connection = Connection::getInstance();
    }

    /**
     * Update prices and send emails
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function execute(): bool
    {
        $this->connection->exec(self::MOVE_NEW_TO_OLD);
        $insert  = $this->connection->prepare(self::INSERT_ADVERTS);
        $select  = $this->connection->query(self::SELECT_ADVERTS);
        $adverts = $select->fetchAll(PDO::FETCH_ASSOC);

        $insert->bindParam(':new_price', $newPrice);
        $insert->bindParam(':id', $id);

        try {
            $this->connection->beginTransaction();

            foreach ($adverts as &$advert) {
                $newPrice = $this->request->execute($advert['advert']);
                $id       = $advert['id'];
                $insert->execute();
            }

            $this->connection->commit();
        } catch (PDOException) {
            $this->connection->rollBack();
            return false;
        }

        $emails = $this->connection->query(self::SELECT_EMAILS);
        $emails = $emails->fetchAll(PDO::FETCH_ASSOC);

        foreach ($emails as $email) {
            $this->sender->execute($email['email'], $email['new_price']);
        }

        return true;
    }
}
