<?php

return [
    'db' => [
        'name' => 'parser',
        'user' => 'parser',
        'pass' => '',
        'host' => '127.0.0.1',
        'port' => '3306',
    ],
    'olx_selector_start' => '\"displayValue\":\"',
    'olx_selector_end'   => '\",\"regularPrice\":',
    'email' => [
        'email_from' => 'vitya.3485@gmail.com',
    ],
];
