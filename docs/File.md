## File structure

### DB folder
- Contain Connection.php singleton class which used every where to connect to db

### Entity folder
- Contain classes for subscription. 
- Abstract class contain common methods for two classes 
- Other files make one insert query and one to select id and create many to many connection

### Services folder
- Subscribe class implement subscription logic
- CronTask class move new column to old column than it update prices and send emails

### Service/Cron folder
- RequestPrice class make request to website to get new price
- SendEmail send email letters with new price
  ![alt text](images/mailhog.png)
- ![alt text](images/emails.png)

### config.php
- contains different configs
- ![alt text](images/config.png)

### migration.php
- this file is responsible for database creation

### subscribe.php
- responsible for subscription from terminal
```shell
php subscribe.php ttt@gmail.com https://olx.ua/your-advert-com
```

### index.php
- responsible for subscription from http
```shell
php -S 127.0.0.1:8000 index.php
```
Query
```shell
curl --location --request GET 'http://127.0.0.1:8000/index.php?email=tttt%40gmail.com&advert=https%3A%2F%2Fwww.olx.ua%2Fd%2Fuk%2Fobyavlenie%2Fkolpaki-2112-abs-plastik-IDTqo0u.html%3Freason%3Dip%7Clister'
```

### cron.php
- This file run CronTask class
```shell
php cron.php
```