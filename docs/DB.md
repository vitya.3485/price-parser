## Database configurations

#### Login in mysql as sudo user
```shell
sudo mysql
```

#### Create database
```mysql
CREATE DATABASE IF NOT EXISTS parser;
```

#### Create user
```mysql
CREATE USER 'parser'@'localhost';
```

#### Give all permissions for local development
```mysql
GRANT ALL PRIVILEGES ON * . * TO 'parser'@'localhost';
```

#### Login in mysql
```shell
mysql -u parser
```
#### Set configurations to config.php
```php
<?php

return [
	'db' => [
		'name' => 'parser',
		'user' => 'parser',
		'pass' => '',
		'host' => '127.0.0.1',
		'port' => '3306',
	],
];
```
#### Run PHP migration
It will create table with many to many connection. And unique columns and keys in tables advert, email, email_advert 
```shell
php migration.php
```
It will run this SQL query
```sql
CREATE TABLE IF NOT EXISTS email (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS advert (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    advert INT UNSIGNED NOT NULL UNIQUE,
    old_price FLOAT,
    new_price FLOAT
);

CREATE TABLE IF NOT EXISTS email_advert (
    email_id INT UNSIGNED NOT NULL,
    advert_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (email_id) REFERENCES email(id) ON DELETE CASCADE,
    FOREIGN KEY (advert_id) REFERENCES advert(id) ON DELETE CASCADE,
    UNIQUE KEY email_id_advert_id(email_id, advert_id)
);
```