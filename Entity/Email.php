<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Entity;

/**
 * Advert entity class
 *
 * @author vitya.3485@gmail.com
 */
class Email extends AbstractEntity
{
    /**
     * Sql queries
     */
    protected const SELECT = 'SELECT id FROM email WHERE email = :email;';
    protected const INSERT = 'INSERT INTO email (email) VALUES (:email);';

    /**
     * Insert only unique emails and get id
     *
     * @param string $email
     *
     * @return int
     */
    public function save(string $email): int
    {
        [$select, $insert] = $this->prepare(self::INSERT, self::SELECT);

        $insert->bindParam(':email', $email);
        $select->bindParam(':email', $email);

        $this->insert($insert);

        return $this->select($select);
    }
}
