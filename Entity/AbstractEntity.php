<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Entity;

use Viktor\OlxParser\DB\Connection;
use PDOStatement;
use PDOException;

/**
 * Advert entity class
 *
 * @author vitya.3485@gmail.com
 */
abstract class AbstractEntity
{
    /**
     * Prepare queries
     *
     * @param string $insert
     * @param string $select
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    protected function prepare(string $insert, string $select): array
    {
        $connection = Connection::getInstance();
        $insert     = $connection->prepare($insert);
        $select     = $connection->prepare($select);

        return [$select, $insert];
    }

    /**
     * Save if it is possible
     *
     * @param PDOStatement $insert
     *
     * @return void
     */
    protected function insert(PDOStatement $insert): void
    {
        try {
            $insert->execute();
        } catch (PDOException $e) {
            ;// it throws error if value will be not unique
        }
    }

    /**
     * Select value if exist else return -1
     *
     * @param PDOStatement $select
     *
     * @return int
     */
    protected function select(PDOStatement $select): int
    {
        try {
            $select->execute();
            return (int) $select->fetchColumn();
        } catch (PDOException) {
            return -1;
        }
    }
}
