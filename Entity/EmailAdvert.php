<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Entity;

use Viktor\OlxParser\DB\Connection;

/**
 * Advert entity class
 *
 * @author vitya.3485@gmail.com
 */
class EmailAdvert extends AbstractEntity
{
    /**
     * Sql queries
     */
    protected const INSERT = 'INSERT INTO email_advert (email_id, advert_id) VALUES (:email_id, :advert_id);';

    /**
     * Insert only unique email_id <-> advert_id combination
     *
     * @param int $emailId
     * @param int $advertId
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function save(int $emailId, int $advertId): void
    {
        $connection = Connection::getInstance();
        $insert     = $connection->prepare(self::INSERT);

        $insert->bindParam(':email_id', $emailId);
        $insert->bindParam(':advert_id', $advertId);
        $this->insert($insert);
    }
}
