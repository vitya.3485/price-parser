<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Entity;

/**
 * Advert entity class
 *
 * @author vitya.3485@gmail.com
 */
class Advert extends AbstractEntity
{
    /**
     * Sql queries
     */
    protected const SELECT = 'SELECT id FROM advert WHERE advert = :advert;';
    protected const INSERT = 'INSERT INTO advert (advert, old_price, new_price) VALUES (:advert, :old_val, :new_val);';

    /**
     * Insert only unique adverts and get id
     *
     * @param string $link
     * @param string $old
     * @param string $new
     *
     * @return int
     */
    public function save(string $link, string $old, string $new): int
    {
        [$select, $insert] = $this->prepare(self::INSERT, self::SELECT);

        $insert->bindParam(':advert', $link);
        $insert->bindParam(':old_val', $old);
        $insert->bindParam(':new_val', $new);
        $select->bindParam(':advert', $link);

        $this->insert($insert);

        return $this->select($select);
    }
}
