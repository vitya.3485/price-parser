<?php

/**
 * Subscribe via terminal
 */

include 'vendor/autoload.php';

use Viktor\OlxParser\Services\Subscribe;

if (isset($argv[1], $argv[2])) {
    $email  = $argv[1];
    $advert = $argv[2];

    $subscribe = new Subscribe();
    $result    = $subscribe->execute($email, $advert);

    if (!$result) {
        echo 'Something went wrong' . PHP_EOL;
    }
} else {
    echo 'The arguments is not specified' . PHP_EOL;
}
