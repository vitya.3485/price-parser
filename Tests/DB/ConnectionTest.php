<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Tests\DB;

use Viktor\OlxParser\DB\Connection;
use PHPUnit\Framework\TestCase;

/**
 * Connection singleton test
 *
 * @author vitya.3485@gmail.com
 */
class ConnectionTest extends TestCase
{
    /**
     * Test to check if connection is not changed
     *
     * @return void
     */
    public function testUniqueness(): void
    {
        $firstCall = Connection::getInstance();
        $secondCall = Connection::getInstance();

        $this->assertSame($firstCall, $secondCall);
    }
}
