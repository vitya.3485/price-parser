<?php

declare(strict_types=1);

namespace Viktor\OlxParser\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Viktor\OlxParser\Entity\AbstractEntity;
use Viktor\OlxParser\Entity\Advert;
use Viktor\OlxParser\Entity\Email;
use Viktor\OlxParser\Entity\EmailAdvert;

/**
 * Advert entity test class
 *
 * @author vitya.3485@gmail.com
 */
class AbstractTest extends TestCase
{
    /**
     * @dataProvider entityProvider
     *
     * @param object $entity
     *
     * @return void
     */
    public function testEntity(object $entity): void
    {
        $this->assertInstanceOf(AbstractEntity::class, $entity);
        $this->assertTrue(method_exists($entity, 'save'));
        $this->assertTrue(method_exists($entity, 'select'));
        $this->assertTrue(method_exists($entity, 'insert'));
        $this->assertTrue(method_exists($entity, 'prepare'));
    }

    /**
     * Provider with class entities
     *
     * @return string[]
     */
    public static function entityProvider(): array
    {
        return [
            [new Advert()],
            [new Email()],
            [new EmailAdvert()],
        ];
    }
}
