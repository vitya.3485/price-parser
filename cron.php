<?php

/**
 * File for cron price update
 */

include 'vendor/autoload.php';

use Viktor\OlxParser\Services\CronTask;

$object = new CronTask();

$object->execute();
