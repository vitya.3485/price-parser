<?php

/**
 * File for web endpoint
 */

include 'vendor/autoload.php';

use Viktor\OlxParser\Services\Subscribe;

if (isset($_GET['email'], $_GET['advert'])) {
    $email  = urldecode($_GET['email']);
    $advert = urldecode($_GET['advert']);

    $subscribe = new Subscribe();
    $result    = $subscribe->execute($email, $advert);

    if (!$result) {
        echo 'Something went wrong';
    } else {
        echo 'You were subscribed';
    }
} else {
    echo 'The arguments is not specified';
}
