#! /bin/bash

echo "PHPCS"
php vendor/bin/phpcs -s --ignore="*vendor/*" --colors --standard=PSR12,PSR2 .

echo "PHPMD"
php vendor/bin/phpmd DB,Entity,Services,index.php,config.php,cron.php,migration.php,subscribe.php text cleancode,codesize,controversial,design,naming,unusedcode --color

echo "PHPSTAN"
php vendor/bin/phpstan analyse -l 4 DB Entity Services config.php index.php subscribe.php migration.php cron.php

echo "PHPUNIT"
php vendor/bin/phpunit Tests