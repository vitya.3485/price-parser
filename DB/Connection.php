<?php

declare(strict_types=1);

namespace Viktor\OlxParser\DB;

use PDO;

/**
 * Singleton for database connection
 */
class Connection
{
    /**
     * Database dns
     */
    private const CONNECTION = 'mysql:dbname=%1$s;host=%2$s:%3$s';

    /**
     * @var PDO|null
     */
    private static ?PDO $instance = null;

    /**
     * Constructor
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private function __construct()
    {
    }

    /**
     * Main singleton method for getting connection
     *
     * @return PDO
     */
    public static function getInstance(): PDO
    {
        if (self::$instance === null) {
            $configs  = include __DIR__ . '/../config.php';
            $configs  = $configs['db'];
            $dsn      = sprintf(self::CONNECTION, $configs['name'], $configs['host'], $configs['port']);
            $instance = new PDO($dsn, $configs['user'], $configs['pass']);

            $instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance = $instance;
        }

        return self::$instance;
    }

    /**
     * Forbidden method
     *
     * @return void
     */
    private function __clone(): void
    {
    }

    /**
     * Forbidden method
     *
     * @return void
     */
    public function __wakeup(): void
    {
    }
}
